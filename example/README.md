## An Easter Egg trigger widget

- Please follow this link to find a working and concrete example :
<a href="https://gitlab.com/mrdiez/flutter_scaffold" target="_blank">The Flutter Scaffold by Mr#</a>
And look at the **./lib/components/images/main.logo.dart** file

- It can't be easier to use :
```
    EasterEggTrigger(
      action: () => print("Easter Egg !!!"),
      child: Image.network('https://picsum.photos/250?image=9'),
      codes: [
        EasterEggTriggers.SwipeLeft,
        EasterEggTriggers.SwipeUp,
        EasterEggTriggers.SwipeRight,
        EasterEggTriggers.SwipeDown,
      ],
    );
```

This example will print **Easter Egg !!!** in your console
when you do these *gestures* on the image:

  - >Swipe Left, Swipe Up, Swipe Right, Swipe Down
