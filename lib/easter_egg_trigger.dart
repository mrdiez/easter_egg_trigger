library easter_egg_trigger;

import 'package:flutter/material.dart';

///
/// The [EasterEggTrigger] widget allow you to easily call a function
/// when a combination of gestures have been detected on a given widget
///
/// Please find a working and concrete example here: https://gitlab.com/mrdiez/flutter_scaffold
/// in the ./lib/components/images/main.logo.dart file
///
/// Sample call :
/// ```dart
///     EasterEggTrigger(
///       action: () => print("Easter Egg !!!"),
///       child: Image.network('https://picsum.photos/250?image=9')
///     );
/// ```
///
/// This example will print "Easter Egg !!!" in your console
/// when you do these gestures on the image:
///
///   Swipe Up, Swipe Up, Swipe Down, Swipe Down,
///   Swipe Left, Swipe Right, Swipe Left, Swipe Right,
///   Tap, Long Press
///
/// Yes this is the famous Konami code: ↑ ↑ ↓ ↓ ← → ← → B A
/// note that I replaced B button by Tap and A button by Long Press
///
class EasterEggTrigger extends StatefulWidget {

  ///
  /// The action that will be executed
  ///
  final Function? action;

  ///
  /// The child on which the gestures will be listened to
  ///
  final Widget? child;

  ///
  /// The list of gestures to be carried out to call action()
  ///
  final List<EasterEggTriggers> codes;

  ///
  /// The constructor
  ///
  EasterEggTrigger({this.action, this.child, codes})
      : this.codes = codes ?? KonamiCode;

  ///
  /// The default list of gestures (the famous Konami code)
  ///
  static const List<EasterEggTriggers> KonamiCode = [
    EasterEggTriggers.SwipeUp, EasterEggTriggers.SwipeUp,
    EasterEggTriggers.SwipeDown, EasterEggTriggers.SwipeDown,
    EasterEggTriggers.SwipeLeft, EasterEggTriggers.SwipeRight,
    EasterEggTriggers.SwipeLeft, EasterEggTriggers.SwipeRight,
    EasterEggTriggers.Tap, EasterEggTriggers.LongPress,
  ];

  @override
  _EasterEggTriggerState createState() => _EasterEggTriggerState();
}

class _EasterEggTriggerState extends State<EasterEggTrigger> {

  ///
  /// The last date where a gesture have been detected on the given child
  ///
  DateTime? lastCodeDate;

  ///
  /// The maximum delay between two gestures
  ///
  Duration _lastCodeExpiration = Duration(seconds: 2);

  ///
  /// The getter to find out if the last gesture is older than the maximum delay
  ///
  bool get isLastCodeExpired => lastCodeDate == null || DateTime.now().difference(lastCodeDate!).compareTo(_lastCodeExpiration) > 0;

  ///
  /// Last vertical gesture detected
  ///
  EasterEggTriggers? _lastVerticalDrag;

  ///
  /// Last vertical gesture starting ordinate
  ///
  late double _lastVerticalDragStart;

  ///
  /// Last horizontal gesture detected
  ///
  EasterEggTriggers? _lastHorizontalDrag;

  ///
  /// Last horizontal gesture starting abscissas
  ///
  late double _lastHorizontalDragStart;

  ///
  /// Current list of non expired gestures
  ///
  List<EasterEggTriggers?> _currentCodes = [];

  ///
  /// Called when all gestures have been detected
  ///
  void onCode() => widget.action != null ? widget.action!() : null;

  ///
  /// Called when a vertical gesture is starting
  ///
  void onVerticalDragStart(DragStartDetails details) {
    _lastVerticalDragStart = details.globalPosition.dy;
  }

  ///
  /// Called when a vertical gesture is in progress
  ///
  void onVerticalDragUpdate(DragUpdateDetails details) {
    _lastVerticalDrag = details.globalPosition.dy > _lastVerticalDragStart
        ? EasterEggTriggers.SwipeDown : EasterEggTriggers.SwipeUp;
  }

  ///
  /// Called when a vertical gesture is stopped
  ///
  void onVerticalDragEnd(DragEndDetails details) => onGesture(_lastVerticalDrag);

  ///
  /// Called when a horizontal gesture is starting
  ///
  void onHorizontalDragStart(DragStartDetails details) {
    _lastHorizontalDragStart = details.globalPosition.dx;
  }

  ///
  /// Called when a horizontal gesture is in progress
  ///
  void onHorizontalDragUpdate(DragUpdateDetails details) {
    _lastHorizontalDrag = details.globalPosition.dx > _lastHorizontalDragStart
        ? EasterEggTriggers.SwipeRight : EasterEggTriggers.SwipeLeft;
  }

  ///
  /// Called when a horizontal gesture is stopped
  ///
  void onHorizontalDragEnd(DragEndDetails details) => onGesture(_lastHorizontalDrag);

  ///
  /// Called when a gesture is detected
  ///
  void onGesture(EasterEggTriggers? code) {
    if (isLastCodeExpired) _currentCodes = [];
    _currentCodes.add(code);
    lastCodeDate = DateTime.now();
    if (_currentCodes.length >= widget.codes.length) {
      int i = 0;
      bool isCode = true;
      List<EasterEggTriggers?> lastCodes = [..._currentCodes].getRange(_currentCodes.length - widget.codes.length, _currentCodes.length).toList();
      lastCodes.forEach((code) {
        isCode = isCode && code == widget.codes[i];
        i++;
      });
      if (isCode) onCode();
    }
  }

  ///
  /// The rendered view
  ///
  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onVerticalDragStart: onVerticalDragStart,
      onVerticalDragUpdate: onVerticalDragUpdate,
      onVerticalDragEnd: onVerticalDragEnd,
      onHorizontalDragStart: onHorizontalDragStart,
      onHorizontalDragUpdate: onHorizontalDragUpdate,
      onHorizontalDragEnd: onHorizontalDragEnd,
      onTap: () => onGesture(EasterEggTriggers.Tap),
      onLongPress: () => onGesture(EasterEggTriggers.LongPress),
      child: widget.child,
    );
  }
}


///
/// Available gestures list
///
enum EasterEggTriggers {
  SwipeUp,
  SwipeDown,
  SwipeLeft,
  SwipeRight,
  Tap,
  LongPress
}